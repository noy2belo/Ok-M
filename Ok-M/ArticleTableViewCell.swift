//
//  ArticleTableViewCell.swift
//  Ok-M
//
//  Created by admin on 27/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

 
    @IBOutlet weak var title: UILabel!
    

    
    @IBOutlet weak var articleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
