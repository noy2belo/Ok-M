//
//  Article.swift
//  Ok-M
//
//  Created by admin on 25/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Article{
    
    var id:String
    var category:String
    var imageURL:String
    var title:String
    var info:String
    var creator_id:String
    var comments:[Comment]?
    var lastUpdate:Date?
    
    init(category:String, title:String, info:String, creator_id:String, comments:[Comment]?, id : String = "-1", imageURL : String = "default") {
        self.category = category
        self.imageURL = imageURL
        self.title = title
        self.info = info
        self.creator_id = creator_id
        self.comments = comments
        
        if id == "-1"{
            self.id = String(title.hashValue)
        }
        else{
            self.id = id
        }
    }
    init(json:Dictionary<String,Any>) {
        self.id = json["id"] as! String
        self.category = json["category"] as! String
        self.imageURL = json["imageURL"] as! String
        self.title = json["title"] as! String
        self.info = json["info"] as! String
        self.creator_id = json["creator_id"] as! String
        self.comments = nil
        if let ts = json["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }
    }
    func toJson() -> [String:Any] {
        var json = [String:Any]()
        json["id"] = id
        json["category"] = category
        json["imageURL"] = imageURL
        json["title"] = title
        json["info"] = info
        json["creator_id"] = creator_id
        //json["comments"] = comments
        json["lastUpdate"] = ServerValue.timestamp()
        
        return json
    }
    func toFirebase() -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["id"] = id
        json["category"] = category
        json["imageURL"] = imageURL
        json["title"] = title
        json["info"] = info
        json["creator_id"] = creator_id
        // json["comments"] = comments
        json["lastUpdate"] = ServerValue.timestamp()
        return json
    }
    init(fromJson:[String:Any]) {
        self.id = fromJson["id"] as! String
        self.category = fromJson["category"] as! String
        self.imageURL = fromJson["imageURL"] as! String
        self.title = fromJson["title"] as! String
        self.info = fromJson["info"] as! String
        self.creator_id = fromJson["creator_id"] as! String
        self.comments = nil
        if let ts = fromJson["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }
       
    }
}
