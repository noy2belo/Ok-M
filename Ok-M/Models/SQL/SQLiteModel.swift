//
//  Model.swift
//  StoreIt
//
//  Created by admin on 02/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class SQLiteModel{
    static let instance = SQLiteModel()
    var database: OpaquePointer? = nil
    init() {
        // open DB
        let dbFileName = "OkM.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{ let path = dir.appendingPathComponent(dbFileName)
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK { print("Failed to open db file: \(path.absoluteString)")
                return
                
            }; print ("im here")}
    }
    /// TODO: add creating tables
    
    static func add_new_user(oDB database:OpaquePointer?,user:User){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database, "INSERT OR REPLACE INTO user VALUES (?,?,?,?,?,?)",-1,
                               &sqlite3_stmt,nil) == SQLITE_OK){
            let id = Int32(user.id)
            let username = user.username.cString(using: .utf8)
            let password = user.password.cString(using: .utf8)
            let mail = user.mail.cString(using: .utf8)
            let birthday = user.birthday.cString(using: .utf8) // TODO: check how to insert date and bind by what?
            let sex = user.sex.cString(using: .utf8)
            sqlite3_bind_int(sqlite3_stmt, 1, id);
            sqlite3_bind_text(sqlite3_stmt, 2, username,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, password,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, mail,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, birthday,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, sex,-1,nil);
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
    }
    static func add_inventory(toDB database:OpaquePointer?,inventory:Inventory){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database, "INSERT OR REPLACE INTO inventory VALUES (?,?)",-1,
                               &sqlite3_stmt,nil) == SQLITE_OK){
            let id = Int32(inventory.id)
            let creator = Int32(5)
            
            sqlite3_bind_int(sqlite3_stmt, 1, id);
            sqlite3_bind_int(sqlite3_stmt, 2, creator);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
    }
    static func add_user_to_inventory(toDB database:OpaquePointer?,inventory_id:Int,user_id:Int){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database, "INSERT OR REPLACE INTO user_and_inventory VALUES (?,?)",-1,
                               &sqlite3_stmt,nil) == SQLITE_OK){
            let inventory_id = Int32(inventory_id)
            let user_id = Int32(user_id)
            
            
            sqlite3_bind_int(sqlite3_stmt, 2, inventory_id);
            sqlite3_bind_int(sqlite3_stmt, 1, user_id);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
    }
    // if i have more then 1 count for the same product, this func will call as many as the counter
    // same for remove product that will insert end_time
    static func add_product_to_inventory(toDB database:OpaquePointer?,object_id:Int,inventory_id:Int,client_product:ClientProduct){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database, "INSERT OR REPLACE INTO product_to_inventory VALUES (?,?,?,?,?,?)",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            let object_id = Int32(object_id)
            let inventory_id = Int32(inventory_id)
            let profuct_id = Int32(client_product.product.id)
            let user_id = Int32(client_product.user_insert_id)
            let insert_time = client_product.insert_time.cString(using: .utf8) // TODO: covert to?!
            var end_time = "".cString(using: .utf8)
            if let close = client_product.end_time {// TODO: covert to?!
                end_time = close.cString(using: .utf8)
            }
            // let avg_time_to_end = 1.5 // not in the db anymore
            sqlite3_bind_int(sqlite3_stmt, 1, object_id);
            sqlite3_bind_int(sqlite3_stmt, 2, inventory_id);
            sqlite3_bind_int(sqlite3_stmt, 3, profuct_id);
            sqlite3_bind_int(sqlite3_stmt, 4, user_id); //TODO: to change the bing for Date
            sqlite3_bind_text(sqlite3_stmt, 5, insert_time,-1,nil); //TODO: to change the bing for Date
            sqlite3_bind_text(sqlite3_stmt, 6, end_time, -1, nil); //TODO: to change the bing for Date
            // sqlite3_bind_double(sqlite3_stmt, 6, avg_time_to_end);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
    }
    /// product wrapps:
    // get product types
    
    static func getProductType(toDB database:OpaquePointer!, withId id:Int)->String?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from product_type where id = ?;\0",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                
                return name
            }
        }
        NSLog("error %s", sqlite3_errmsg(database))
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    // get product unit of measure
    
    static func getProductUnitOfMeasure(fromDB database:OpaquePointer?, withId id:Int)->String?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from unit_of_measure where id = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                
                return name
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    ///// getting sub_Catrgory!
    
    static func getSubCatrgory(fromDB database:OpaquePointer?, withId id:Int)->SubCategory?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from sub_category where id = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                
                
                return SubCategory(id: id,name: name)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    /// get all sub category by Category id
    static func getAllSubCatrgoryByCategoryID(fromDB database:OpaquePointer?, withId category_id:Int)->[SubCategory]?{
        var sqlite3_stmt: OpaquePointer? = nil
        var subs = [SubCategory]()
        while(sqlite3_prepare_v2(database,"SELECT * from sub_category where father_category = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(category_id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                
                
                subs.append(SubCategory(id: id,name: name))
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return subs
    }
    ///// getting Catrgory!
    
    static func getCatrgory(fromDB database:OpaquePointer?, withId id:Int)->Category?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from main_category where id = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                
                let subs = getAllSubCatrgoryByCategoryID(fromDB: database, withId: id)
                return Category(id: id,name: name, sub_categories:subs)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    ///// getting Product!
    
    static func getProductByBarcode(fromDB database:OpaquePointer?, withId barcode:Int)->Product?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from product where barcode = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(barcode));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let type  = Int(sqlite3_column_int(sqlite3_stmt,1))
                let brand  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let sub_category_id  = Int(sqlite3_column_int(sqlite3_stmt,4))
                let percent  = Int(sqlite3_column_int(sqlite3_stmt,5))
                let unitOfMeasure_id  = Int(sqlite3_column_int(sqlite3_stmt,6))
                let unit  = Int(sqlite3_column_int(sqlite3_stmt,7))
                let package  = String(cString:sqlite3_column_text(sqlite3_stmt,8))
                let barcode  = Int(sqlite3_column_int(sqlite3_stmt,9))
                
                
                let type_string = getProductType(toDB: database, withId: type)
                let sub_category = getSubCatrgory(fromDB: database, withId: sub_category_id)
                let unit_of_measure_string = getProductUnitOfMeasure(fromDB: database, withId: unitOfMeasure_id)
                return Product(id:id,type:(type_string)!, brand:brand,name:name, sub_category:sub_category!,percent:percent,unit_amount:unit_of_measure_string!,amount:unit, package:package,barcode:barcode)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    static func getProductIDByBarcode(fromDB database:OpaquePointer?, withId barcode:Int)->Int?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from product where barcode = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(barcode));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                
                
                
                return id
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    static func getProductById(fromDB database:OpaquePointer?, withId id:Int)->Product?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from product where id = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let type  = Int(sqlite3_column_int(sqlite3_stmt,1))
                let brand  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let sub_category_id  = Int(sqlite3_column_int(sqlite3_stmt,4))
                let percent  = Int(sqlite3_column_int(sqlite3_stmt,5))
                let unitOfMeasure_id  = Int(sqlite3_column_int(sqlite3_stmt,6))
                let unit  = Int(sqlite3_column_int(sqlite3_stmt,7))
                let package  = String(cString:sqlite3_column_text(sqlite3_stmt,8))
                let barcode  = Int(sqlite3_column_int(sqlite3_stmt,9))
                
                
                let type_string = getProductType(toDB: database, withId: type)
                let sub_category = getSubCatrgory(fromDB: database, withId: sub_category_id)
                let unit_of_measure_string = getProductUnitOfMeasure(fromDB: database, withId: unitOfMeasure_id)
                return Product(id:id,type:(type_string)!, brand:brand,name:name, sub_category:sub_category!,percent:percent,unit_amount:unit_of_measure_string!,amount:unit, package:package,barcode:barcode)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    static func getInventoryClientProducts_by_id(fromDB database:OpaquePointer?, withId inventoey_id:Int)->[ClientProduct]?{
        var sqlite3_stmt: OpaquePointer? = nil
        var clientProducts = [ClientProduct]()
        if (sqlite3_prepare_v2(database,"SELECT * from product_to_inventory where inventory_id = ?;" ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(inventoey_id));
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id = Int(sqlite3_column_int(sqlite3_stmt,0))
                let inventory_id  = Int(sqlite3_column_int(sqlite3_stmt,1))
                let product_id  = Int(sqlite3_column_int(sqlite3_stmt,2))
                let user_id  = Int(sqlite3_column_int(sqlite3_stmt,3))
                let insert_time  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let end_time =  String(cString:sqlite3_column_text(sqlite3_stmt,5))
                
                let product = getProductById(fromDB: database, withId: product_id)
                
                
                clientProducts.append(ClientProduct(product:product!,insert_time:insert_time, end_time:end_time, user_insert_id:user_id,id:id))
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return clientProducts
    }
    
    static func getInventories_by_user_id(fromDB database:OpaquePointer?, withId user_id:Int)->[Inventory]?{
        var sqlite3_stmt: OpaquePointer? = nil
        var inventories = [Inventory]()
        if (sqlite3_prepare_v2(database,"SELECT * from user_and_inventory where user_id = ?;" ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(user_id));
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let inventory_id  = Int(sqlite3_column_int(sqlite3_stmt,1))
                
                let all_connected_products = getInventoryClientProducts_by_id(fromDB: database, withId: inventory_id)
                inventories.append(Inventory(id:inventory_id, all_connected_products:all_connected_products)) // TODO: to add the inventories
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return inventories
    }
    /// get user by id
    
    static func getUserById(fromDB database:OpaquePointer?, withId id:Int)->User?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from user where id = ?;" ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_int(sqlite3_stmt, 1, Int32(id));
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let username  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let birthday  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let sex  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
                
                let users_inventories = getInventories_by_user_id(fromDB: database, withId: id)
                return User(id: id,username: username,password: password, mail: mail, birthday:birthday, sex:sex, inventories: users_inventories)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    static func getUserByName(fromDB database:OpaquePointer?, withName name:String)->User?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from user where username = ?;" ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1,name, -1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let username  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let birthday  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let sex  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
                
                let users_inventories = getInventories_by_user_id(fromDB: database, withId: id)
                return User(id: id,username: username,password: password, mail: mail, birthday:birthday, sex:sex, inventories: users_inventories)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    /// get user by name and password
    
    static func getUserByNameAndPassword(fromDB database:OpaquePointer?, withName name:String, withPassword password:String)->User?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from user where username = ? and password = ?;" ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, name.cString(using: .utf8),-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, password.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,0))
                let username  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let birthday  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let sex  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
                
                let users_inventories = getInventories_by_user_id(fromDB: database, withId: id)
                return User(id: id,username: username,password: password, mail: mail, birthday:birthday, sex:sex, inventories: users_inventories)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    /// get last sequence from db
    
    static func getLastSequence(fromDB database:OpaquePointer?, withtable tablename:String)->Int?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from sqlite_sequenece where name = ? " ,-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, tablename.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = Int(sqlite3_column_int(sqlite3_stmt,2))
                /// todo: to check if its 2 or 1
                return id
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    
    
}

