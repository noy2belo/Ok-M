//
//  Article sql.swift
//  Ok-M
//
//  Created by admin on 25/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
extension Comment{
    static let TABLE = "COMMENTS"
    static let ID = "ID"
    static let ARTICLE_ID = "ARTICLE_ID"
    static let TIMESTAMP = "TIMESTAMP"
    static let CREATOR_ID = "CREATOR_ID"
    static let CONTEXT = "CONTEXT"
    static let LAST_UPDATE = "LAST_UPDATE"
    
    static func createTable(toDB database:OpaquePointer?)->Bool{
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + TABLE + " (" +
            ID + " TEXT PRIMARY KEY, " +
            ARTICLE_ID + " TEXT, " +
            TIMESTAMP + " DOUBLE, " +
            CREATOR_ID + " TEXT, " +
            CONTEXT + " TEXT, " +
            LAST_UPDATE + " DOUBLE)", nil, nil, &errormsg)
        if(res != 0){
            print("error creating table");
            return false
        }
        return true
    }
    
    func addNewCommentToLocalDb(toDB database:OpaquePointer?){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            Comment.TABLE + " ( " +
            Comment.ID + ", " +
            Comment.ARTICLE_ID + ", " +
            Comment.TIMESTAMP + ", " +
            Comment.CREATOR_ID + ", " +
            Comment.CONTEXT + ", " +
             Comment.LAST_UPDATE + " ) VALUES (?,?,?,?,?,?);",-1,
                                              &sqlite3_stmt,nil) == SQLITE_OK){
            
            
            let id = self.id.cString(using: .utf8)
            let article_id = self.article_id.cString(using: .utf8)
            let creator_id = self.creator_id.cString(using: .utf8)
            let context = self.context.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, article_id,-1,nil);
            
            sqlite3_bind_double(sqlite3_stmt, 3, self.timeStamp!.toFirebase());
            sqlite3_bind_text(sqlite3_stmt, 4, creator_id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, context,-1,nil);
            
            if (lastUpdate == nil){
                lastUpdate = Date()
            }
            sqlite3_bind_double(sqlite3_stmt, 6, lastUpdate!.toFirebase());
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    static func getAllCommentsFromLocalDb(fromDB database:OpaquePointer?)->[Comment]{
        var comments = [Comment]()
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + ";",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let article_id  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let timeStamp =  Double(sqlite3_column_double(sqlite3_stmt,2))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let context  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,5))
                var new_comment = Comment(article_id: article_id, timeStamp: Date.fromFirebase(timeStamp), creator_id: creator_id, context: context, id: id)
                new_comment.lastUpdate = Date.fromFirebase(update)
                comments.append(new_comment)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return comments
    }
//    static func getAllCommentsByArticleIDFromLocalDb(withId Id:String, fromDB database:OpaquePointer?)->[Comment]{
//        var comments = [Comment]()
//        var sqlite3_stmt: OpaquePointer? = nil
//        if (sqlite3_prepare_v2(database,"SELECT * from " +
//            TABLE + " where "+ARTICLE_ID+" = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
//
//            sqlite3_bind_text(sqlite3_stmt, 1,Id.cString(using: .utf8),-1,nil);
//
//            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
//                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
//                let article_id  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
//                let timeStamp =  Double(sqlite3_column_double(sqlite3_stmt,2))
//                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
//                let context  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
//                let update =  Double(sqlite3_column_double(sqlite3_stmt,5))
//                var new_comment = Comment(article_id: article_id, timeStamp: Date.fromFirebase(timeStamp), creator_id: creator_id, context: context, id: id)
//                new_comment.lastUpdate = Date.fromFirebase(update)
//                comments.append(new_comment)
//            }
//        }
//        sqlite3_finalize(sqlite3_stmt)
//        return comments
//    }
    static func getCommentByArticleIdFromLocalDb(fromDB database:OpaquePointer?, withId artId:String)->[Comment]{
        var sqlite3_stmt: OpaquePointer? = nil
        var comments3 = [Comment]()
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + " where " +
            ARTICLE_ID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, artId.cString(using: .utf8),-1,nil);
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let article_id  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let timeStamp =  Double(sqlite3_column_double(sqlite3_stmt,2))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let context  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,5))
                var new_comment = Comment(article_id: article_id, timeStamp: Date.fromFirebase(timeStamp), creator_id: creator_id, context: context, id: id)
                                new_comment.lastUpdate = Date.fromFirebase(update)
                                comments3.append(new_comment)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return comments3
    }
    static func getCommentById(fromDB database:OpaquePointer?, withId Id:String)->Comment?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + " where " +
            ID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1,Id.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let article_id  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let timeStamp  = Double(sqlite3_column_double(sqlite3_stmt,2))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let context  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,5))
                
                let new_comment = Comment(article_id: article_id, timeStamp: Date.fromFirebase(timeStamp), creator_id: creator_id, context: context, id: id)
                new_comment.lastUpdate = Date.fromFirebase(update)
                return new_comment
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
}

