//
//  LiteModel.swift
//  Ok-M
//
//  Created by admin on 25/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation


extension String {
    public init?(validatingUTF8 cString: UnsafePointer<UInt8>) {
        if let (result, _) = String.decodeCString(cString, as: UTF8.self,
                                                  repairingInvalidCodeUnits: false) {
            self = result
        }
        else {
            return nil
        }
    }
}

class LiteModel{
    var database: OpaquePointer? = nil
    
    init?(){
        let dbFileName = "OkM.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return nil
            }
        }
        
        if Acount.createTable(toDB: database) == false{
            return nil
        }
        if Article.createTable(toDB: database) == false{
            return nil
        }
        if Comment.createTable(toDB: database) == false{
            return nil
        }
        if LastUpdateTable.createTable(database: database) == false{
            return nil
        }
    }
    
}

