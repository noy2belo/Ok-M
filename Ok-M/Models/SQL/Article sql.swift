//
//  Article sql.swift
//  Ok-M
//
//  Created by admin on 25/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
extension Article{
    static let TABLE = "ARTICLES"
    static let ID = "ID"
    static let CATEGORY = "CATEGORY"
    static let IMAGE_URL = "IMAGE_URL"
    static let TITLE = "TITLE"
    static let INFO = "INFO"
    static let CREATOR_ID = "CREATOR_ID"
    static let LAST_UPDATE = "LAST_UPDATE"
    
    static func createTable(toDB database:OpaquePointer?)->Bool{
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + TABLE + " (" +
            ID + " TEXT PRIMARY KEY, " +
            CATEGORY + " TEXT, " +
            IMAGE_URL + " TEXT, " +
            TITLE + " TEXT, " +
            INFO + " TEXT, " +
            CREATOR_ID + " TEXT, " +
            LAST_UPDATE + " DOUBLE)", nil, nil, &errormsg)
        if(res != 0){
            print("error creating table");
            return false
        }
        return true
    }
    
    func addNewArticleToLocalDb(toDB database:OpaquePointer?){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            Article.TABLE + " ( " +
            Article.ID + ", " +
            Article.CATEGORY + ", " +
            Article.IMAGE_URL + ", " +
            Article.TITLE + ", " +
            Article.INFO + ", " +
            Article.CREATOR_ID + ", " +
            Article.LAST_UPDATE + " ) VALUES (?,?,?,?,?,?,?);",-1,
                                        &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = self.id.cString(using: .utf8)
            let category = self.category.cString(using: .utf8)
            let imageURL = self.imageURL.cString(using: .utf8)
            let title = self.title.cString(using: .utf8)
            let info = self.info.cString(using: .utf8)
            let creator_id = self.creator_id.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, category,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, imageURL,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, title,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, info,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, creator_id,-1,nil);
            
            if (self.lastUpdate == nil){
                self.lastUpdate = Date()
            }
            sqlite3_bind_double(sqlite3_stmt, 7, self.lastUpdate!.toFirebase());
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    static func getAllarticles(fromDB database:OpaquePointer?)->[Article]{
        var Articles = [Article]()
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + ";",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let category  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let imageURL  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let title  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let info  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
               let update =  Double(sqlite3_column_double(sqlite3_stmt,6))
                var new_article = Article(category: category, title: title, info: info, creator_id: creator_id, comments: nil, id: id,imageURL: imageURL)
                new_article.lastUpdate = Date.fromFirebase(update)
                Articles.append(new_article)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return Articles
    }
    
    static func getArticlesByCategory(fromDB database:OpaquePointer?, with category:String)->[Article]{
        var sqlite3_stmt: OpaquePointer? = nil
        var arts = [Article]()
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + " where " +
            CATEGORY + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1,category.cString(using: .utf8),-1,nil);
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let category  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let imageURL  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let title  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let info  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,6))
                var new_article = Article(category: category, title: title, info: info, creator_id: creator_id, comments: nil, id: id,imageURL: imageURL)
                new_article.lastUpdate = Date.fromFirebase(update)
                arts.append(new_article)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return arts
    }
    static func getArticlesByUserId(fromDB database:OpaquePointer?, with user_id1:String)->[Article]{
        var sqlite3_stmt: OpaquePointer? = nil
        var arts = [Article]()
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            TABLE + " where " +
            CREATOR_ID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1,user_id1.cString(using: .utf8),-1,nil);
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let category  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let imageURL  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let title  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let info  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let creator_id  = String(cString:sqlite3_column_text(sqlite3_stmt,5))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,6))
                var new_article = Article(category: category, title: title, info: info, creator_id: creator_id, comments: nil, id: id,imageURL: imageURL)
                new_article.lastUpdate = Date.fromFirebase(update)
                arts.append(new_article)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return arts
    }
    static func getAllCategories(fromDB database:OpaquePointer?)->[String]{
        var categories = [String]()
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT distinct(CATEGORY) from " +
            TABLE + ";",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let category  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                categories.append(category)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return categories
    }
    
}
