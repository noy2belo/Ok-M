//
//  FirebaseModel.swift
//  StoreIt
//
//  Created by admin on 19/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseStorage


class FirebaseModel{
    
    // Adding to tables
    static func add_new_user(user:Acount){
        let ref = Database.database().reference()
        let my_ref = ref.child("Users").child(user.id)
        my_ref.setValue(user.toFirebase())
    }
    static func add_Article(article:Article){
        var ref = Database.database().reference()
        let my_ref = ref.child("Articles").child(article.id)
        my_ref.setValue(article.toFirebase())
    }
    
    static func add_comment(comment:Comment){
        var ref = Database.database().reference()
        let my_ref = ref.child("Comments").child(comment.id)
        my_ref.setValue(comment.toFirebase())
    }
    
    // Get from tables
    static func getArticleById(withId id:String,callback:@escaping(Article?)->Void){
        var ref = Database.database().reference()
        let my_ref = ref.child("Articles").child(id)
        my_ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let val = snapshot.value as? [String:Any] {
                let article_from_db = Article(fromJson: val)
                callback(article_from_db)
            }
            else{
                callback(nil)
            }
        })
    }
    static func getUserById(withId id:String, callback:@escaping(Acount?)->Void){
        var ref = Database.database().reference()
        let my_ref = ref.child("users").child(id)
        my_ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let val = snapshot.value as? [String:Any] {
                    let user_from_db = Acount(fromJson: val)
                    callback(user_from_db)
                }
            else{
                callback(nil)
            }
        })
        
        
    }
    static func getAllComments(_ lastUpdateDate:Date? , callback:@escaping ([Comment])->Void){
        print("FB: getAllComments")
        let handler = {(snapshot:DataSnapshot) in
            var comments = [Comment]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let comment = Comment(json: json)
                        comments.append(comment)
                    }
                }
            }
            callback(comments)
        }
        
        let ref = Database.database().reference().child("Comments")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observeSingleEvent(of: .value, with: handler)
        }else{
            ref.observeSingleEvent(of: .value, with: handler)
        }
    }
    static func getAllCommentsAndObserve(_ lastUpdateDate:Date?, callback:@escaping ([Comment])->Void){
        print("FB: getAllCommentsAndObserve")
        let handler = {(snapshot:DataSnapshot) in
            var comments = [Comment]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let comment = Comment(json: json)
                        comments.append(comment)
                    }
                }
            }
            callback(comments)
        }
        
        let ref = Database.database().reference().child("Comments")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observe(DataEventType.value, with: handler)
        }else{
            ref.observe(DataEventType.value, with: handler)
        }
    }
    static func clearObserversFromComments(){
        let ref = Database.database().reference().child("Comments")
        ref.removeAllObservers()
    }
//
    // observers and all objects
        //users
    static func getAllUsers(_ lastUpdateDate:Date? , callback:@escaping ([Acount])->Void){
        print("FB: getAllUsers")
        let handler = {(snapshot:DataSnapshot) in
            var users = [Acount]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let user = Acount(json: json)
                        users.append(user)
                    }
                }
            }
            callback(users)
        }
        
        let ref = Database.database().reference().child("Users")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observeSingleEvent(of: .value, with: handler)
        }else{
            ref.observeSingleEvent(of: .value, with: handler)
        }
    }
    
    static func getAllUsersAndObserve(_ lastUpdateDate:Date?, callback:@escaping ([Acount])->Void){
        print("FB: getAllUsersAndObserve")
        let handler = {(snapshot:DataSnapshot) in
            var users = [Acount]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let user = Acount(json: json)
                        users.append(user)
                    }
                }
            }
            callback(users)
        }
        
        let ref = Database.database().reference().child("Users")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observe(DataEventType.value, with: handler)
        }else{
            ref.observe(DataEventType.value, with: handler)
        }
    }
    static func clearObserversFromUsers(){
        let ref = Database.database().reference().child("Users")
        ref.removeAllObservers()
    }
    
    
    // articles
    
    static func getAllArticles(_ lastUpdateDate:Date? , callback:@escaping ([Article])->Void){
        print("FB: getAllArticles")
        let handler = {(snapshot:DataSnapshot) in
            var articles = [Article]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let article = Article(json: json)
                        articles.append(article)
                    }
                }
            }
            callback(articles)
        }
        
        let ref = Database.database().reference().child("Articles")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observeSingleEvent(of: .value, with: handler)
        }else{
            ref.observeSingleEvent(of: .value, with: handler)
        }
    }
    
    static func getAllArticlesAndObserve(_ lastUpdateDate:Date?, callback:@escaping ([Article])->Void){
        print("FB: getAllArticlesAndObserve")
        let handler = {(snapshot:DataSnapshot) in
            var articles = [Article]()
            for child in snapshot.children.allObjects{
                if let childData = child as? DataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let article = Article(json: json)
                        articles.append(article)
                    }
                }
            }
            callback(articles)
        }
        
        let ref = Database.database().reference().child("Articles")
        if (lastUpdateDate != nil){
            print("q starting at:\(lastUpdateDate!) \(lastUpdateDate!.toFirebase())")
            let fbQuery = ref.queryOrdered(byChild:"lastUpdate").queryStarting(atValue:lastUpdateDate!.toFirebase())
            fbQuery.observe(DataEventType.value, with: handler)
        }else{
            ref.observe(DataEventType.value, with: handler)
        }
    }
    static func clearObserversFromArticles(){
        let ref = Database.database().reference().child("Articles")
        ref.removeAllObservers()
    }
    static func clearObservers(){
        self.clearObserversFromUsers()
        self.clearObserversFromArticles()
        self.clearObserversFromComments()
    }
    /// Auth firebase
    static func Authentication(mail:String, password:String, callback:@escaping (String?) -> Void?) {
        
        
    }
    static func login(mail:String,password:String, callback:@escaping (String?)->Void?){
        var acount_id:String? = nil
        Auth.auth().signIn(withEmail: mail, password: password, completion: { (user, error) in
            if (error != nil) {
                callback(nil)
            }
            else{
                acount_id = user?.uid
                 callback((user?.uid)!)
            }
        })
        
    }
}


