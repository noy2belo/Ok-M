//
//  User SQL.swift
//  Ok-M
//
//  Created by admin on 25/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

extension Acount{
        static let USER_TABLE = "USERS"
        static let ID = "ID"
        static let PASSWORD = "PASSWORD"
        static let MAIL = "MAIL"
        static let LAST_UPDATE = "LAST_UPDATE"
    
    static func createTable(toDB database:OpaquePointer?)->Bool{
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + USER_TABLE + " (" +
            ID + " TEXT PRIMARY KEY, " +
            PASSWORD + " TEXT, " +
            MAIL + " TEXT, " +
            LAST_UPDATE + " DOUBLE)", nil, nil, &errormsg)
        if(res != 0){
            print("error creating table");
            return false
        }
        return true
    }
    
    func addNewUser(toDB database:OpaquePointer?){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            Acount.USER_TABLE + " ( " +
            Acount.ID + ", " +
            Acount.PASSWORD + ", " +
            Acount.MAIL + ", " +
            Acount.LAST_UPDATE + " ) VALUES (?,?,?,?);",-1,
                                         &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = self.id.cString(using: .utf8)
            let password = self.password.cString(using: .utf8)
            let mail = self.mail.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, password,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, mail,-1,nil);
            
            if (self.lastUpdate == nil){
                self.lastUpdate = Date()
            }
            sqlite3_bind_double(sqlite3_stmt, 4, self.lastUpdate!.toFirebase());
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    static func getAllUsersFromLocalDB(fromDB database:OpaquePointer?)->[Acount]{
        var users = [Acount]()
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            USER_TABLE + ";",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,3))
                var new_user = Acount(password: password, mail: mail, articles: nil, id: id)
                new_user.lastUpdate = Date.fromFirebase(update)
                users.append(new_user)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return users
    }
    
    static func getUserById(fromDB database:OpaquePointer?, withId Id:String)->Acount?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            USER_TABLE + " where " +
            ID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1,Id.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let update =  Double(sqlite3_column_double(sqlite3_stmt,3))
                var new_user = Acount(password: password, mail: mail, articles: nil, id: id)
                new_user.lastUpdate = Date.fromFirebase(update)
                return new_user
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    static func getUserByMail(fromDB database:OpaquePointer?, withMail mail:String)->Acount?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            USER_TABLE + " where " +
            MAIL + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, mail.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let password  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let mail  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                
                let update =  Double(sqlite3_column_double(sqlite3_stmt,3))
                var new_user = Acount(password: password, mail: mail, articles: nil, id: id)
                new_user.lastUpdate = Date.fromFirebase(update)
                return new_user
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
}
