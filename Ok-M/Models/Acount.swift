//
//  User.swift
//  StoreIt
//
//  Created by admin on 01/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import Firebase


class Acount{
    
    var id:String
    var password:String
    var mail:String
    var articles:[Article]?
    var lastUpdate:Date?
    
    init(password:String,mail:String,articles:[Article]?,id:String = "-1") {
        self.id = id
        self.password = password
        self.mail = mail
        self.articles = articles
        if id == "-1"{
            self.id = String(mail.hashValue)
        }
        else{
            self.id = id
        }
    }
    init(json:Dictionary<String,Any>) {
        self.id = json["id"] as! String
        self.password = json["password"] as! String
        self.mail = json["mail"] as! String
        self.articles = json["articles_objects"] as? [Article]
        if let ts = json["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }
    }
    func toJson() -> [String:Any] {
        var json = [String:Any]()
        json["id"] = id
        json["password"] = password
        json["mail"] = mail
        json["lastUpdate"] = ServerValue.timestamp()
        
        
        return json
    }
    func toFirebase() -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["id"] = id
        json["password"] = password
        json["mail"] = mail
        json["lastUpdate"] = ServerValue.timestamp()
        
        return json
    }
    init(fromJson:[String:Any]) {
        self.id = fromJson["id"] as! String
        self.password = fromJson["password"] as! String
        self.mail = fromJson["mail"] as! String
        self.articles = fromJson["articles_objects"] as? [Article]
        if let ts = fromJson["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }
       
    }
}

