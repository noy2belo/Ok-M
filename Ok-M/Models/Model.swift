//
//  Model.swift
//  Ok-M
//
//  Created by admin on 25/01/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

class Model{
    
    static let instance = Model()
    lazy private var modelSql:LiteModel? = LiteModel()
    
    // Manage funcs
    private init(){
    }
    func clear(){
        print("Model.clear")
        FirebaseModel.clearObservers()
    }
    // Auth
//    func Auth(mail: String, password: String)->String{
//        var id:String? = nil
//        let myGroup = DispatchGroup()
//        myGroup.enter()
//        FirebaseModel.Authentication(mail: mail, password: password, callback: {(user) in
//           id = user
//            myGroup.leave()
//        })
//        myGroup.notify(queue: .main){
//            return id
//        }
//
//    }
//    func login(mail:String,password:String)->String?{
//        var id:String? = nil
//        let myGroup = DispatchGroup()
//        myGroup.enter()
//        FirebaseModel.login(mail: mail,password: password, callback: {(user) in
//            id = user
//            myGroup.leave()
//            })
//        myGroup.notify(queue: .main){
//        return id
//        }
//    
//    }
    // add funcs
    func addUser(user:Acount){
        FirebaseModel.add_new_user(user: user)
    }
    func addArticle(article:Article){
        FirebaseModel.add_Article(article: article)
    }
    func addComment(comment:Comment){
        FirebaseModel.add_comment(comment: comment)
    }
    func getUserByName(name:String)->Acount?{

       return Acount.getUserByMail(fromDB: modelSql?.database, withMail: name)
    }
    
    func getArticlesByCategory(with category:String)->[Article]{
       let artsFromDb = Article.getArticlesByCategory(fromDB: modelSql?.database, with: category)
        for articlefromDb in artsFromDb{
            let article_comments = self.getCommentByArticleID(withId: articlefromDb.id)
            articlefromDb.comments = article_comments
        }
        return artsFromDb
    }
    func getArticlesByUserId(with user_id1:String)->[Article]{
        let artsFromDb =  Article.getArticlesByUserId(fromDB: modelSql?.database, with: user_id1)
        for articlefromDb in artsFromDb{
            let article_comments = self.getCommentByArticleID(withId: articlefromDb.id)
            articlefromDb.comments = article_comments
        }
        return artsFromDb
    }
    func getUserById(id:String)->Acount?{
        var userFromDb = Acount.getUserById(fromDB: modelSql?.database, withId: id)
            var articles:[Article] = []
            articles = Model.instance.getArticlesByUserId(with: id)
        userFromDb?.articles = articles
        return userFromDb
    }
    func getCommentByArticleID(withId id:String)->[Comment]{
        return Comment.getCommentByArticleIdFromLocalDb(fromDB: self.modelSql?.database, withId: id)
        
    }
    func getAllCategories()->[String?]{
        let categories = Article.getAllCategories(fromDB: self.modelSql?.database)
        return categories
    }
    func getAllComments(callback:@escaping([Comment]?)->Void){
        print("Model.getAllComments")
        
        // get last update date from SQL
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: modelSql?.database, table: Comment.TABLE)
        
        // get all updated records from firebase
        FirebaseModel.getAllComments(lastUpdateDate, callback: { (comments) in
            //update the local db
            print("got \(comments.count) new records from FB")
            var lastUpdate:Date?
            for comment in comments{
                comment.addNewCommentToLocalDb(toDB: self.modelSql?.database)
                if lastUpdate == nil{
                    lastUpdate = comment.lastUpdate
                }else{
                    if lastUpdate!.compare(comment.lastUpdate!) == ComparisonResult.orderedAscending{
                        lastUpdate = comment.lastUpdate
                    }
                }
            }
            
            //upadte the last update table
            if (lastUpdate != nil){
                LastUpdateTable.setLastUpdate(database: self.modelSql!.database, table: Comment.TABLE, lastUpdate: lastUpdate!)
            }
            
            //get the complete list from local DB
            let totalList = Comment.getAllCommentsFromLocalDb(fromDB: self.modelSql?.database)
            
            //return the list to the caller
            callback(totalList)
        })
    }
    func getAllArticles(callback:@escaping([Article]?)->Void){
        print("getAllArticles")
        
        // get last update date from SQL
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: modelSql?.database, table: Article.TABLE)
        
        // get all updated records from firebase
        FirebaseModel.getAllArticles(lastUpdateDate, callback: { (articles) in
            //update the local db
            print("got \(articles.count) new records from FB")
            var lastUpdate:Date?
            for article in articles{
                article.addNewArticleToLocalDb(toDB: self.modelSql?.database)
                if lastUpdate == nil{
                    lastUpdate = article.lastUpdate
                }else{
                    if lastUpdate!.compare(article.lastUpdate!) == ComparisonResult.orderedAscending{
                        lastUpdate = article.lastUpdate
                    }
                }
            }
            
            //upadte the last update table
            if (lastUpdate != nil){
                LastUpdateTable.setLastUpdate(database: self.modelSql!.database, table: Article.TABLE, lastUpdate: lastUpdate!)
            }
            
            //get the complete list from local DB
            let totalList = Article.getAllarticles(fromDB: self.modelSql?.database)
            for articlefromDb in totalList{
                let article_comments = self.getCommentByArticleID(withId: articlefromDb.id)
                articlefromDb.comments = article_comments
            }
            
            //return the list to the caller
            callback(totalList)
        })
    }
    func getAllUsersAndObserve(){
        print("Model.getAllUsersAndObserve")
        // get last update date from SQL
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: modelSql?.database, table: Acount.USER_TABLE)
        
        // get all updated records from firebase
        FirebaseModel.getAllUsersAndObserve(lastUpdateDate, callback: { (users) in
            //update the local db
            print("got \(users.count) new records from FB")
            var lastUpdate:Date?
            for user in users{
                user.addNewUser(toDB: self.modelSql?.database)
                if lastUpdate == nil{
                    lastUpdate = user.lastUpdate
                }else{
                    if lastUpdate!.compare(user.lastUpdate!) == ComparisonResult.orderedAscending{
                        lastUpdate = user.lastUpdate
                    }
                }
            }
            
            //upadte the last update table
            if (lastUpdate != nil){
                LastUpdateTable.setLastUpdate(database: self.modelSql!.database, table: Acount.USER_TABLE, lastUpdate: lastUpdate!)
            }
            
            //get the complete list from local DB
            let totalList = Acount.getAllUsersFromLocalDB(fromDB: self.modelSql?.database)
            //print("\(totalList)")
            for user in totalList{
                var articles:[Article] = []
                articles = Model.instance.getArticlesByUserId(with: user.id)
                user.articles = articles
            }
            
            ModelNotificationObjects.UserList.post(data: totalList)
        })
    }
    func getAllArticlesAndObserve(){
        print("Model.getAllArticlesAndObserve")
        // get last update date from SQL
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: modelSql?.database, table: Article.TABLE)
        
        // get all updated records from firebase
        FirebaseModel.getAllArticlesAndObserve(lastUpdateDate, callback: { (arts) in
            //update the local db
            print("got \(arts.count) new records from FB")
            var lastUpdate:Date?
            for art in arts{
                art.addNewArticleToLocalDb(toDB: self.modelSql?.database)
                if lastUpdate == nil{
                    lastUpdate = art.lastUpdate
                }else{
                    if lastUpdate!.compare(art.lastUpdate!) == ComparisonResult.orderedAscending{
                        lastUpdate = art.lastUpdate
                    }
                }
            }
            
            //upadte the last update table
            if (lastUpdate != nil){
                LastUpdateTable.setLastUpdate(database: self.modelSql!.database, table: Article.TABLE, lastUpdate: lastUpdate!)
            }
            
            //get the complete list from local DB
            let totalList = Article.getAllarticles(fromDB: self.modelSql?.database)
            print("\(totalList)")
            for articlefromDb in totalList{
                let article_comments = self.getCommentByArticleID(withId: articlefromDb.id)
                articlefromDb.comments = article_comments
            }
            ModelNotificationObjects.ArticleList.post(data: totalList)
        })
    }
    func getAllCommentsAndObserve(){
        print("Model.getAllCommentsAndObserve")
        // get last update date from SQL
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: modelSql?.database, table: Comment.TABLE)
        
        // get all updated records from firebase
        FirebaseModel.getAllCommentsAndObserve(lastUpdateDate, callback: { (comments) in
            //update the local db
            print("got \(comments.count) new records from FB")
            var lastUpdate:Date?
            for comment in comments{
                comment.addNewCommentToLocalDb(toDB: self.modelSql?.database)
                if lastUpdate == nil{
                    lastUpdate = comment.lastUpdate
                }else{
                    if lastUpdate!.compare(comment.lastUpdate!) == ComparisonResult.orderedAscending{
                        lastUpdate = comment.lastUpdate
                    }
                }
            }
            
            //upadte the last update table
            if (lastUpdate != nil){
                LastUpdateTable.setLastUpdate(database: self.modelSql!.database, table: Comment.TABLE, lastUpdate: lastUpdate!)
            }
            
            //get the complete list from local DB
            let totalList = Comment.getAllCommentsFromLocalDb(fromDB: self.modelSql?.database)
            print("\(totalList)")
            
            ModelNotificationObjects.CommentList.post(data: totalList)
        })
    }
    
}
