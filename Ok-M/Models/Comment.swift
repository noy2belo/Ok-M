//
//  Comment.swift
//  Ok-M
//
//  Created by admin on 25/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import Firebase

class Comment{
    
    var id:String
    var article_id:String
    var timeStamp:Date?
    var creator_id:String
    var context:String
    var lastUpdate:Date?
    
    init(article_id:String,timeStamp:Date,creator_id:String,context:String, id:String = "-1") {
        self.id = id
        self.article_id = article_id
        self.timeStamp = timeStamp
        self.creator_id = creator_id
        self.context = context
        
        if id == "-1"{
            self.id = String(context.hashValue)
        }
        else{
            self.id = id
        }
    }
    init(json:Dictionary<String,Any>) {
        self.id = json["id"] as! String
        self.article_id = json["article_id"] as! String
        if let timeWrite = json["timeStamp"] as? Double{
            self.timeStamp = Date.fromFirebase(timeWrite)
        }
        self.creator_id = json["creator_id"] as! String
        self.context = json["context"] as! String
        
        if let ts = json["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }
    
    }
    func toJson() -> [String:Any] {
        var json = [String:Any]()
        json["id"] = id
        json["article_id"] = article_id
        json["timeStamp"] = ServerValue.timestamp()
        json["creator_id"] = creator_id
        json["context"] = context
        json["lastUpdate"] = ServerValue.timestamp()
        
        return json
    }
    func toFirebase() -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["id"] = id
        json["article_id"] = article_id
        json["timeStamp"] = ServerValue.timestamp()
        json["creator_id"] = creator_id
        json["context"] = context
        json["lastUpdate"] = ServerValue.timestamp()
        return json
    }
    init(fromJson:[String:Any]) {
        self.id = fromJson["id"] as! String
        self.article_id = fromJson["article_id"] as! String
        if let timeWrite = fromJson["timeStamp"] as? Double{
            self.timeStamp = Date.fromFirebase(timeWrite)
        }
        self.creator_id = fromJson["creator_id"] as! String
        self.context = fromJson["context"] as! String
        if let ts = fromJson["lastUpdate"] as? Double{
            self.lastUpdate = Date.fromFirebase(ts)
        }

    }
    
}
