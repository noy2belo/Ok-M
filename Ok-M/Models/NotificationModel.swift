//
//  NotificationModel.swift
//  Ok-M
//
//  Created by admin on 28/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class ModelNotificationBase<T>{
    var name:String?
    
    init(name:String){
        self.name = name
    }
    
    func observe(callback:@escaping (T?)->Void)->Any{
        return NotificationCenter.default.addObserver(forName: NSNotification.Name(name!), object: nil, queue: nil) { (data) in
            if let data = data.userInfo?["data"] as? T {
                callback(data)
            }
        }
    }
    
    func post(data:T){
        NotificationCenter.default.post(name: NSNotification.Name(name!), object: self, userInfo: ["data":data])
    }
}

class ModelNotificationObjects{
    static let ArticleList = ModelNotificationBase<[Article]>(name: "ArticleListNotificatio")
    static let Article = ModelNotificationBase<Article>(name: "ArticleNotificatio")
    static let CommentList = ModelNotificationBase<[Comment]>(name: "CommentListNotificatio")
    static let Comment = ModelNotificationBase<Comment>(name: "CommentNotificatio")
     static let UserList = ModelNotificationBase<[Acount]>(name: "UserListNotificatio")
    
    static func removeObserver(observer:Any){
        NotificationCenter.default.removeObserver(observer)
    }
}
