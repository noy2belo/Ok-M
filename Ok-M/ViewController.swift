//
//  ViewController.swift
//  Ok-M
//
//  Created by admin on 27/12/2017.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import Firebase

var user_id:String? = "1"

class ViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // load users and observe
        ModelNotificationObjects.UserList.observe { (list) in
            if list != nil{
                
            }
        }
        
        Model.instance.getAllUsersAndObserve()
      password.placeholder = "Enter password"
    UserName.placeholder = "Enter mail or username"
    }
    

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var UserName: UITextField!
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
            return true
    }
    
    @IBAction func OnloginClick(_ sender: Any) {
        
        if (password.text != "" && UserName.text != ""){
            var passwordInput = password.text
            var mail = UserName.text
            /// Test For firebase auth
//            var id = Model.instance.login(mail:mail!, password:passwordInput!)
//                if id != nil{
//                    user_id = id!
//                }
//
            var userFromDb = Model.instance.getUserByName(name: mail!)
            if (userFromDb != nil){
                if (userFromDb?.mail == mail && userFromDb?.password == passwordInput){
                    user_id = userFromDb?.id
                        performSegue(withIdentifier: "main", sender: self)
                    }
                    else{
                        // alert
                    let alert = UIAlertController(title: "Wrong inputs", message: "Let's try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    // alert for register - cant find mail
                let alert = UIAlertController(title: "You are not register", message: "Please register first", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)

                }
            }
        else{
            let alert = UIAlertController(title: "Some missing inputs", message: "Let's try to add some inputs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

