//
//  ArticleInfoController.swift
//  Ok-M
//
//  Created by admin on 27/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class ArticleInfoController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet weak var commentInput: UITextField!
    var article:Article? = nil
    var refreshCtrl:UIRefreshControl = UIRefreshControl()
    //var commentsTable:UITableView? = nil


    @IBAction func addComment(_ sender: Any) {    
        if commentInput.text != "" {
            let newComment = Comment(article_id: article!.id, timeStamp: Date(), creator_id: user_id!, context: commentInput.text!)
            Model.instance.addComment(comment: newComment)
            commentInput.text = ""
        }
        else{
            // Let the user know that the input is invalid
            
            let alert = UIAlertController(title: "Can't add comment because of a missing input", message: "Let's try to add inputs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let row_count = article!.comments!.count
        return row_count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comCell", for: indexPath) as! CommentTableViewCell
        let commentCreator = Model.instance.getUserById(id: article!.comments![indexPath.row].creator_id)
        if commentCreator != nil{
        cell.commentContent.text = (commentCreator?.mail)! + ":" + article!.comments![indexPath.row].context
        }
        else{
            cell.commentContent.text = ""
        }
        return cell
    }
    
    
    @IBOutlet weak var articleInfo: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        article = articles[articleSelected!]
        articleTitle.text = article?.title
        articleInfo.text = article?.info
       // self.commentInput.becomeFirstResponder()
        let imUrl = article?.imageURL
        if imUrl != "default"{
        ModelFileStore.getImage(urlStr: imUrl!, callback: { (image) in
            self.articleImage.image = image
        })
        }

        ModelNotificationObjects.CommentList.observe { (list) in
            if list != nil{
                self.article?.comments? = []
                for comment in list!{
                    if (comment.article_id == self.article?.id){
                       self.article?.comments?.append(comment)
                    }
                }
                self.commentsTable.reloadData()
            }
        }
        
        Model.instance.getAllCommentsAndObserve()
        
        // refresh spinner
        commentsTable.addSubview(refreshCtrl)
        refreshCtrl.addTarget(self, action: #selector(ArticleInfoController.refreshData), for: UIControlEvents.valueChanged)
    }
    @objc func refreshData(){
        commentsTable.reloadData()
        refreshCtrl.endRefreshing()
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
