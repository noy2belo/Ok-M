//
//  ArticlesController.swift
//  Ok-M
//
//  Created by admin on 27/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit



class ArticlesController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var category:String? = nil
    var observerId:Any?
    var refreshCtrl:UIRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var articlesTable: UITableView!
    
    
    @IBOutlet weak var displayCategory: UILabel!
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let row_count = articles.count
        return row_count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell2", for: indexPath) as! ArticleTableViewCell
        
        let imUrl = articles[indexPath.row].imageURL
        if imUrl != "default"{
            ModelFileStore.getImage(urlStr: imUrl, callback: { (image) in
                cell.articleImage!.image = image
            })
        }
        cell.title.text = articles[indexPath.row].title
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        category = categories[itemSelected!]
        displayCategory.text = category!
        articles = Model.instance.getArticlesByCategory(with: category!)
        
        ModelNotificationObjects.ArticleList.observe { (list) in
            if list != nil{
                articles = []
                for art in list!{
                    if (art.category == self.category){
                        articles.append(art)
                    }
                }
                self.articlesTable?.reloadData()
            }
        }
        
        Model.instance.getAllArticlesAndObserve()
        
        // refresh spinner
        articlesTable.addSubview(refreshCtrl)
        refreshCtrl.addTarget(self, action: #selector(ArticlesController.refreshData), for: UIControlEvents.valueChanged)
    }
    @objc func refreshData(){
        articlesTable.reloadData()
        refreshCtrl.endRefreshing()
    }
    deinit{
        if (observerId != nil){
            ModelNotificationObjects.removeObserver(observer: observerId!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        articleSelected = indexPath.item
        performSegue(withIdentifier: "info", sender: self)
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ArticleInfo"{
//            var dest = segue.destination as! ArticleInfoController
//            dest.article = articles[itemSelected!]
//            dest.user_id_loggedin = self.user_id_loggedin
//        }
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
