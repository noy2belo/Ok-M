//
//  MainController.swift
//  Ok-M
//
//  Created by admin on 30/12/2017.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

var categories:[String?] = []
var itemSelected:Int? = 0
var articles:[Article] = []
var articleSelected:Int? = nil

class MainController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
    var user:Acount? = nil
    var refreshCtrl:UIRefreshControl = UIRefreshControl()
    @IBOutlet weak var collection: UICollectionView!
    // number of views in the collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    // populate the views
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
        cell.name.text = categories[indexPath.row]
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        user = Model.instance.getUserById(id: user_id!)
//        Model.instance.getAllComments{ (comments) in
//            if let result = comments{
//                let comments2 = result
//            }
//        }
//        Model.instance.getAllArticles { (articles2) in
//            if let result = articles2{
//                articles = result
//            }
//        }
//        categories = Model.instance.getAllCategories()
        
        // layoutSet
        let itemSize = UIScreen.main.bounds.width/3 - 3
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 0, 10, 0)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        
        collection.collectionViewLayout = layout
        
        // observers
        
        ModelNotificationObjects.ArticleList.observe { (list) in
            if list != nil{
                articles = list!
                self.collection.reloadData()
                categories = Model.instance.getAllCategories()
            }
        }
        
        Model.instance.getAllArticlesAndObserve()
        
        ModelNotificationObjects.CommentList.observe { (list) in
            if list != nil{
                var is_new = true
                for comm in list!{
                    for art in (self.user?.articles)!{
                        if comm.article_id == art.id{
                            for com in art.comments!{
                                if com.id == comm.id{
                                    is_new = false
                                }
                            }
                            if is_new{
                                let alert = UIAlertController(title: "Someone postes a comment on your article", message: "", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    
                }
            }
        }
        
        Model.instance.getAllCommentsAndObserve()

        
        // refresh spinner
        collection.addSubview(refreshCtrl)
        refreshCtrl.addTarget(self, action: #selector(MainController.refreshData), for: UIControlEvents.valueChanged)
        
        
    }
    @objc func refreshData(){
        collection.reloadData()
        refreshCtrl.endRefreshing()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // if item was selected we want to perform sague
        itemSelected = indexPath.item
        performSegue(withIdentifier: "byCategory", sender: self)
        //
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
