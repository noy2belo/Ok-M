//
//  CreateArticleViewController.swift
//  Ok-M
//
//  Created by admin on 25/02/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class CreateArticleViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    var article:Article?
    var imageUrl:String?
    
    @IBOutlet weak var titleInput: UITextField!
    @IBOutlet weak var categoryInput: UITextField!
    @IBOutlet weak var infoInput: UITextField!
    
    @IBOutlet weak var articleInfo: UILabel!
    @IBOutlet weak var articleCategory: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //user_id = "0"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    @IBAction func save(_ sender: Any) {
        if (infoInput.text != "" && titleInput.text != "" && categoryInput.text != "") {
            article = Article(category: categoryInput.text!, title: titleInput.text!, info: infoInput.text!, creator_id: user_id!, comments: nil)
            if image != nil{
                //save image
                ModelFileStore.saveImage(image: image!, name: article!.id){(url) in
                    // image was stored
                    self.imageUrl = url
                    // update the article image
                    self.article?.imageURL = self.imageUrl!
                    Model.instance.addArticle(article: self.article!)
                    self.navigationController!.popViewController(animated: true)
                }
        }
        
            // save new article in db
            Model.instance.addArticle(article: article!)
            
            }
        else{
            // Let the user know that the input is invalid
            
            let alert = UIAlertController(title: "Can't add article because of a missing inputs", message: "Let's try to add inputs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func picFromGalery(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let ctrl = UIImagePickerController()
            ctrl.delegate = self
            present(ctrl, animated: true, completion: nil)
        }
    }
    
    @IBAction func takePic(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = info["UIImagePickerControllerOriginalImage"] as! UIImage
        self.articleImage.image = image
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
