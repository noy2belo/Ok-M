//
//  RegisterController.swift
//  Ok-M
//
//  Created by admin on 01/03/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import Firebase


class RegisterController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mail: UITextField!
    
    @IBAction func register(_ sender: Any) {
        if (password.text != "" && mail.text != ""){
            var passwordInput = password.text
            var mailOrUsername = mail.text
            var acount_id = Model.instance.getUserByName(name: mailOrUsername!)
            if acount_id != nil{
                let alert = UIAlertController(title: "Can't add this mail", message: "Please try again with other inputs", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
            }
            else{
                Model.instance.addUser(user: Acount(password: passwordInput!, mail: mailOrUsername!, articles: nil))
                
                self.navigationController!.popViewController(animated: true)
                //                    user_id = user?.uid
                //                        performSegue(withIdentifier: "main", sender: self)
            }
            
//            /// Test For firebase auth
//            var acount_id:String? = nil
//            Auth.auth().createUser(withEmail: mailOrUsername!, password: passwordInput!, completion: { (user: User?, error) in
//                if (error != nil){
//                    // cant add this mail
//                    let alert = UIAlertController(title: "Can't add this mail", message: "Please try again with other inputs", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
//                else{
//                    // successfully auth user
//                    Model.instance.addUser(user: Acount(password: passwordInput!, mail: mailOrUsername!, articles: nil, id: user!.uid))
//                    user_id = user?.uid
//                        performSegue(withIdentifier: "main", sender: self)
//                                    }
//
//            })
//            let new_user_id = Model.instance.Auth(mail: mailOrUsername!, password: passwordInput!)
//            if new_user_id == nil{
//                    let alert = UIAlertController(title: "Can't add this mail", message: "Please try again with other inputs", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
//                else{
//                    // successfully auth user
//                Model.instance.addUser(user: Acount(password: passwordInput!, mail: mailOrUsername!, articles: nil, id: new_user_id))
//                user_id = new_user_id
//                    performSegue(withIdentifier: "main", sender: self)
//                }
        }
        else{
            let alert = UIAlertController(title: "Some missing inputs", message: "Let's try to add some inputs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        password.placeholder = "Enter password"
        mail.placeholder = "Enter mail or username"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
